/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {AppRegistry, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import CircularSlider from './src/components/CircularSlider/CircularSlider';

export default class standardRN extends Component {
  constructor(props) {
    super(props);
    this.state = {
      slider1: 270,
      slider2: 180,
    };
  }
  render() {
    return (
      <View style={styles.root}>
        <View style={styles.container}>
          <View style={styles.slider1}>
            <CircularSlider
              width={200}
              height={200}
              meterColor="red"
              textColor="#fff"
              value={this.state.slider1}
              onValueChange={value => this.setState({slider1: value})}
            />
          </View>
          <View style={styles.slider2}>
            <TouchableOpacity onPress={ () => console.log(123) } style={{ borderRadius: 40 }}>
              <View style={{width: 140, height: 140, backgroundColor: 'navy', borderRadius: 70, flex: 1, alignItems: 'center', flexDirection: 'row' }}>
                <Text style={{color:'white', backgroundColor: 'transparent', textAlign: 'center', flex: 1}}>Shoot</Text>
              </View>
            </TouchableOpacity>
            {/*<CircularSlider
              width={150}
              height={150}
              meterColor="#ffa"
              textColor="#000"
              value={this.state.slider2}
              onValueChange={value => this.setState({slider2: value})}
            />*/}
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  container: {
    position: 'relative',
    width: 200,
    height: 200,
  },
  slider1: {
    position: 'absolute',
    top: 0,
    left: 0,
  },
  slider2: {
    zIndex: 0,
    position: 'absolute',
    top: 30,
    left: 30,
  },
});

AppRegistry.registerComponent('standardRN', () => standardRN);
